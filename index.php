<?php
require 'kernek.php';
require 'App/Kiwiz.php';


$url = $_SERVER['REQUEST_URI'];
//dump($_SERVER);
$kiwiz = new Kiwiz();

//dump($kiwiz->contructUrl('/token/generate'));


if('/token' === $url ){


    $token =$kiwiz->getToken();

    return view(include'view/token.php', $token);


}elseif ('/quota' === $url){


     $quota = $kiwiz->quota();

    return compact(include'view/quota.php', $quota);

}elseif ('/invoice' === $url){

    $file = 'CGM-FAC-1053103.pdf';

    $invoices = [
        'increment_id' => 'INV0009290', // Numéro de la facture
        'date' =>  '2018-01-24 18:48:25' , //Date UTC au format Y-m-d H:i:s
        'email' => 'roger@domain.com', // Email associé à l’avoir
        'billing_address' => [
            'firstname'	=> 'Roger', //Prénom
            'lastname' => 'Dupont' ,//Nom
            'company' => 'Mon Entreprise',  //Société
            'street' =>	' 550 chemin de la colombe' ,//Adresse
            'postcode' => 13290, //Code postal
            'city' => 'Aix en Provence' , //Ville
            'country_code'	=> 'FR', //Code pays au format ISO
        ],
        'payment_method' =>'Paypal' , //Methode de paiement
        'shipping_address' => [
            'firstname'	=> 'Roger', //Prénom
            'lastname' => 'Dupont' ,//Nom
            'company' => 'Mon Entreprise',  //Société
            'street' =>	' 550 chemin de la colombe' ,//Adresse
            'postcode' => 13290, //Code postal
            'city' => 'Aix en Provence' , //Ville
            'country_code'	=> 'FR', //Code pays au format ISO
        ],
        'shipping_method' =>  'DPD' , //Méthode de livraison
        'shipping_amount_excl_tax' => 10.0000, //Montant de la livraison hors taxes (séparateur décimale point, 4 décimales)
        'shipping_tax_amount' => [
            300 => [
                'tax_name' =>'TVA 20%', //Nom de la taxe
                'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
            ]
        ],
        'items' =>[
            [
                'sku' => 'REF254', //Référence article
                'ean13' => 9584521547856, //Code EAN13
                'product_name' => 'Aspirateur Rowenta silent force', //Nom du produit
                'manufacturer' => 'Rowenta' , //Fabricant du produit
                'qty' => 10.0000, //Quantité facturé (séparateur décimale point, 4 décimales)
                'row_total_excl_tax' => 100.0000, //Montant hors taxes du total des articles (séparateur décimale point, 4 décimales)
                'row_total_tax_amount' => [
                    200 =>
                        [
                            'tax_name' => 'TVA 20%',  //Nom de la taxe
                            'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                        ],

                ]
            ],

        ],
        'grand_total_excl_tax' => 110.0000, //Montant total de la facture hors taxes (séparateur décimale point, 4 décimales)
        'grand_total_tax_amount' => [
            200 =>
                [
                    'tax_name' => 'TVA 20%',  //Nom de la taxe
                    'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                ],

        ],

    ];


    return compact(include 'view/invoice.php', 'file', 'invoices');

}elseif ('/send_invoice' === $url){


    $kiwiz = new Kiwiz();


    $file = 'CGM-FAC-1053103.pdf';
//dump($file);

    $invoice = [
        'increment_id' => 'INV0009290', // Numéro de la facture
        'date' =>  '2018-01-24 18:48:25' , //Date UTC au format Y-m-d H:i:s
        'email' => 'roger@domain.com', // Email associé à l’avoir
        'billing_address' => [
            'firstname'	=> 'Roger', //Prénom
            'lastname' => 'Dupont' ,//Nom
            'company' => 'Mon Entreprise',  //Société
            'street' =>	' 550 chemin de la colombe' ,//Adresse
            'postcode' => 13290, //Code postal
            'city' => 'Aix en Provence' , //Ville
            'country_code'	=> 'FR', //Code pays au format ISO
        ],
        'payment_method' =>'Paypal' , //Methode de paiement
        'shipping_address' => [
            'firstname'	=> 'Roger', //Prénom
            'lastname' => 'Dupont' ,//Nom
            'company' => 'Mon Entreprise',  //Société
            'street' =>	' 550 chemin de la colombe' ,//Adresse
            'postcode' => 13290, //Code postal
            'city' => 'Aix en Provence' , //Ville
            'country_code'	=> 'FR', //Code pays au format ISO
        ],
        'shipping_method' =>  'DPD' , //Méthode de livraison
        'shipping_amount_excl_tax' => 10.0000, //Montant de la livraison hors taxes (séparateur décimale point, 4 décimales)
        'shipping_tax_amount' => [
            300 => [
                'tax_name' =>'TVA 20%', //Nom de la taxe
                'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
            ]
        ],
        'items' =>[
            [
                'sku' => 'REF254', //Référence article
                'ean13' => 9584521547856, //Code EAN13
                'product_name' => 'Aspirateur Rowenta silent force', //Nom du produit
                'manufacturer' => 'Rowenta' , //Fabricant du produit
                'qty' => 10.0000, //Quantité facturé (séparateur décimale point, 4 décimales)
                'row_total_excl_tax' => 100.0000, //Montant hors taxes du total des articles (séparateur décimale point, 4 décimales)
                'row_total_tax_amount' => [
                    200 =>
                        [
                            'tax_name' => 'TVA 20%',  //Nom de la taxe
                            'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                        ],

                ]
            ],

        ],
        'grand_total_excl_tax' => 110.0000, //Montant total de la facture hors taxes (séparateur décimale point, 4 décimales)
        'grand_total_tax_amount' => [
            200 =>
                [
                    'tax_name' => 'TVA 20%',  //Nom de la taxe
                    'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                ],

        ],

    ];







    $saveInvoice = $kiwiz->setInvoice($file, $invoice);

    return compact(include 'view/send_invoice.php','saveInvoice');




}elseif ('/link_invoice' === $url){


  //  dd($_POST['block']);
     //header('Content-type: application/pdf');

     $pdf =   $kiwiz->getInvoice($_POST['block']);
     dd($pdf);

//return   view(include'view/view_pdf.php', $pdf);




}elseif ('/avoir' === $url){

        $avoir = [

            'increment_id' => 'INV000985', //Numéro de l’avoir
            'date' => '2018-01-24 18:48:25', //Date UTC au format Y-m-d H:i:s
            'grand_total_excl_tax' =>  100.0000, //Montant total de l’avoir hors taxes (séparateur décimale point, 4 décimales)
            'grand_total_tax_amount'=>	[
                200 =>
                    [
                        'tax_name' => 'TVA 20%',  //Nom de la taxe
                        'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                    ],
            ],
            'email' => 'roger@domain.com', //Email associé à l’avoir


        ];

    $file = 'CGM-FAC-1053103.pdf';

        return compact(include'view/avoir.php','file', 'avoir');



}elseif ('/send_avoir' === $url ){

    $avoir = [

        'increment_id' => 'INV000985', //Numéro de l’avoir
        'date' => '2018-01-24 18:48:25', //Date UTC au format Y-m-d H:i:s
        'grand_total_excl_tax' =>  100.0000, //Montant total de l’avoir hors taxes (séparateur décimale point, 4 décimales)
        'grand_total_tax_amount'=>	[
            200 =>
                [
                    'tax_name' => 'TVA 20%',  //Nom de la taxe
                    'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                ],
        ],
        'email' => 'roger@domain.com', //Email associé à l’avoir


    ];
    $file = 'CGM-FAC-1053103.pdf';

    $kiwiz = new Kiwiz();
    $saveAvoir= $kiwiz->setAvoir($file, $avoir);
    return compact(include'view/send_avoir.php','saveAvoir');

}elseif ('/link_avoir' === $url){

    header('Content-type: application/pdf');

    echo  $kiwiz->getAvoir($_POST['block']);

}










































