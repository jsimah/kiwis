<?php

$curl = curl_init();
switch ($method) {
    case "POST":
        curl_setopt($curl, CURLOPT_POST, 1);
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
    case "PUT":
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
    case "DELETE":
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
    default:
        if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
}
// OPTIONS:
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
if($headers === true) {
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Authorization:'.$signature,
        'Content-Type: application/json',
    ));
}
$result = curl_exec($curl);
$response = null;
if($result) {
    $response = array(
        'statusCode' => curl_getinfo($curl, CURLINFO_HTTP_CODE),
        'data' => json_decode($result)
    );
} elseif (!$result) {
    $response = array(
        'statusCode' => 503,
        'data' => 'Service Unavailable - Connection API Failure'
    );
}
curl_close($curl);
return $response;
}