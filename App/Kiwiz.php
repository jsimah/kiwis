<?php
require 'ClientCurl.php';


class Kiwiz   {

        private $token;

        private $api;

        private $baseUrl ;

        private $baseQuery =[];

        private $query = [];

        private $reponse;
  

        public function __construct()
        {
            $this->setBaseUrl(config('kiwiz.baseUrl'));
            $this->setApi(new \App\ClientCurl($this->getBaseUrl()));
            $this->setBaseQuery(config('kiwiz.platform'), config('kiwiz.version'), config('kiwiz.test_mode'));
            $this->generateToken(config('kiwiz.username'),config('kiwiz.password'),config('kiwiz.subscribId'));
        }

        /**
         * REF : Générer un token d'authentification
         * METHODE : POST
         * URL : /token/generate
         *
         * @param string $username
         * @param string $password
         * @param string $subscribeId
         *
         * @return array
         */
        public function generateToken($username, $password, $subscribeId)
        {
            /*

            $token =  $this->getApi()->post('/token/generate?'.http_build_query($this->getBaseQuery()), [
                'username' => $username,
                'password' => $password ,
                'subscription_id' => $subscribeId
            ]);


            if ($token['status'] == 200) {

                dump($token);
                return  $this->setToken($token['reponse']->token);

            } else {

                dump($token['erreur']);
                return 'eRRor : '. $token['status']. '- message : '.$token['erreur'];
            }
        /*
             *
             *
             */

            $this->setToken(config('kiwiz.token'));

            return $this->getToken();

        }

        /**
         *  REF : cupérer les informations de quota de requette (Ressource : User)
         *  Method : GET
         *  URL : /quota/info
         *
         * @return mixed|string
         */
        public  function quota()
        {
            $this->getApi()->setHeader("accept: application/json");
            $this->getApi()->setHeader('Authorization: '.$this->getToken());

            $this->httpCode($this->getApi()->get($this->urlMode('/quota/info')));


            return $this->getReponse();

        }

        /**
         *  REF : Récupérer une facture (Ressource : User)
         *  METHODE : GET
         *  URL : /invoice/get
         *
         * @param $file
         * @param $invoice
         *
         * @return mixed
         */
        public function setInvoice($file,$invoice)
        {
            $this-> fusionQuery([
                'document' => $this->makeCurlFile($file),
                'data' => json_encode($invoice)]);

            $this->getApi()->setHeader('Authorization: '.$this->getToken());

            $this->httpCode( $this->getApi()->file($this->urlMode('/invoice/save'),$this->getQuery()));

            return $this->getReponse();

        }

        /**
         *  REF : Enregistrer une facture (Ressource : User)
         *  METHODE : POST
         *  URL : /invoice/save
         *
         * @param $hash
         *
         * @return mixed
         */
        public function getInvoice($hash)
        {
            $this->getApi()->setHeader('Authorization: '.$this->getToken());
            $this->httpCode($this->getApi()->get($this->urlMode('/invoice/get', true),['block_hash' => $hash]));

            return $this->getReponse();

        }

        /**
         *  REF : Récupérer un avoir (Ressource : User)
         *  METHODE : POST
         *  URL : /invoice/save
         *
         * @param $file
         * @param $avoir
         *
         * @return mixed
         *
         */
        public function setAvoir($file,$avoir)
        {

            $this-> fusionQuery([
                'document' => $this->makeCurlFile($file),
                'data' => $this->datasInvoice($avoir)]);

            $this->getApi()->setHeader('Authorization: '.$this->getToken());


            return json_decode($this->getApi()->file($this->urlMode('/creditmemo/save'),$this->getQuery())['reponse']);

        }

        /**
         *  REF : Récupérer un avoir (Ressource : User)
         *  METHODE : GET
         *  URL : /creditmemo/get
         *
         * @param $hash
         * @return mixed
         */
         public function getAvoir($hash)
         {
             $this->getApi()->setHeader('Content-type: application/pdf');
             $this->getApi()->setHeader('Content-Disposition: attachment; filename="new.pdf"');
             $this->getApi()->setHeader('Authorization: '.$this->getToken());

             $this->httpCode($this->getApi()->get($this->urlMode('/creditmemo/get', true),['block_hash' => $hash]));

             return $this->getReponse();

        }


        public function datasInvoice($query)
        {
            $invoice = json_decode($query);




            $invoiceAt = [
                'increment_id' => , // Numéro de la facture
                'date' =>  '2018-01-24 18:48:25' , //Date UTC au format Y-m-d H:i:s
                'email' => 'roger@domain.com', // Email associé à l’avoir
                'billing_address' => [
                    'firstname'	=> 'Roger', //Prénom
                    'lastname' => 'Dupont' ,//Nom
                    'company' => 'Mon Entreprise',  //Société
                    'street' =>	' 550 chemin de la colombe' ,//Adresse
                    'postcode' => 13290, //Code postal
                    'city' => 'Aix en Provence' , //Ville
                    'country_code'	=> 'FR', //Code pays au format ISO
                ],
                'payment_method' =>'Paypal' , //Methode de paiement
                'shipping_address' => [
                    'firstname'	=> 'Roger', //Prénom
                    'lastname' => 'Dupont' ,//Nom
                    'company' => 'Mon Entreprise',  //Société
                    'street' =>	' 550 chemin de la colombe' ,//Adresse
                    'postcode' => 13290, //Code postal
                    'city' => 'Aix en Provence' , //Ville
                    'country_code'	=> 'FR', //Code pays au format ISO
                ],
                'shipping_method' =>  'DPD' , //Méthode de livraison
                'shipping_amount_excl_tax' => 10.0000, //Montant de la livraison hors taxes (séparateur décimale point, 4 décimales)
                'shipping_tax_amount' => [
                    300 => [
                        'tax_name' =>'TVA 20%', //Nom de la taxe
                        'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                    ]
                ],
                'items' =>[$items


                ],
                'grand_total_excl_tax' => 110.0000, //Montant total de la facture hors taxes (séparateur décimale point, 4 décimales)
                'grand_total_tax_amount' => [
                    200 =>
                        [
                            'tax_name' => 'TVA 20%',  //Nom de la taxe
                            'tax_value' => 9584521547856, //Montant de la taxe (séparateur décimale point, 4 décimales)
                        ],

                ],

            ];

            return json_encode($invoiceAt);


        }



    /**
     *  REF : Permet de transférer un fichier avec curl
     *
     * @param $file
     * @return CURLFile
     */
    private function makeCurlFile($file)
    {
        $info = pathinfo($file);
        $mime = mime_content_type($file);
        $name = $info['basename'];
        $output = new CURLFile($file, $mime, $name);

        return $output;
    }


        public function urlMode($url, $test_mode=null)
        {


                if(!is_null($test_mode)) {

                    if ($test_mode == false) {
                        return $url.'?';
                    } else {
                       return $url.'?'. http_build_query($this->getBaseQuery()).'&';
                    }


                } else {

                    if ($this->getBaseQuery()['test_mode'] == 0){
                        return $url.'?';
                    } else {
                        return $url.'?'. http_build_query($this->getBaseQuery());
                    }

                }

        }

    /**
     * @return mixed
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * @param mixed $reponse
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;
    }


    private function httpCode($reponse, $status=null, $message=null)
    {
        if ($reponse['status'] === 200) {
            $this->setReponse(json_decode( $reponse['reponse']));


        } else {


            if (!is_null($status)) {
                if ($reponse['status'] == $status && !is_null($message)) {
                    $this->setReponse('Erreur: '. $reponse['status']. ' '. $message);
                }
            }

            $this->setReponse(json_decode($reponse['erreur']));

        }

    }


    /**
     * @param $query
     *
     * @return  array
     */

    public function fusionQuery($query)
    {
        $this->setQuery($query);

    }

    /**
     * @return mixed
     */
    public function getBaseQuery()
    {
        return $this->baseQuery;
    }

    /**
     * @param mixed $baseQuery
     */
    public function setBaseQuery($platform, $version,$test_mode)
    {
        $this->baseQuery = [
                'platform' => $platform,
                'version' => $version,
                'test_mode' => $test_mode
        ];
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
       $this->query =$query ;
    }


    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return mixed
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param mixed $api
     */
    public function setApi($api)
    {
        $this->api = $api;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

   
 
}