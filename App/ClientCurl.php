<?php

namespace App;



class ClientCurl
{
    private $baseUrl;

    private $curl;

    private $headers = [];

    private $options = [];

    private $codeHttp;

    private $errors;

    private $result;

    private $response;


    /**
     * Initialise la base de l'url
     * @param string $$baseUrl
     *
     */
    public function __construct(string $baseUrl)
    {
        $this->init($baseUrl);
    }

    /**
     *  Initialise la base de l'url
     *  Supprime curl
     *  Reinitialise $option a un array vide
     *
     * @param string $baseUrl
     */
    public function init(string $baseUrl)
    {
        $this->setBaseUrl($baseUrl);

        unset($this->curl);
        $this->options = array();
    }

    /**
     *
     *  Appel curl avec methode get
     *  avec query
     *
     * @param  string $url
     * @param array|null $data
     * @return json
     *
     */
    public function get($url, $data=null)
    {

        dump($url.urldecode(http_build_query($data)));

        $this->init($this->getBaseUrl());

        $this->setOption(CURLOPT_CUSTOMREQUEST, 'GET');

        if (!is_null($data)) {
            $this->setOption(CURLOPT_URL, $this->getBaseUrl().$url.urldecode(http_build_query($data)));
        } else {
            $this->setOption(CURLOPT_URL, $this->getBaseUrl().$url);
        }


        $this->exec();
        return $this->getResponse();
    }

    /**
     *  Appel curl avec methode POST
     *  postfield attache
     *
     * @param $url
     * @param array $data
     * @return mixed
     *
     */
    public function post($url, $data)
    {
        $this->init($this->getBaseUrl());

        $this->setHeader( "Content-Type: multipart/form-data");
        $this->setOption(CURLOPT_CUSTOMREQUEST, 'POST');
        $this->setOption(CURLOPT_URL, $this->getBaseUrl().$url);
        $this->setOption(CURLOPT_POSTFIELDS, $data);

        $this->exec();
        return $this->getResponse();
    }

    /**
     *  Appel curl avec methode Post File
     *  envoie de fichier + data POSSTFIELD
     *
     * @param string $url
     * @param  array $data
     * @return mixed
     *
     */
    public function file(string $url, array $data)
    {
        $this->init($this->getBaseUrl());
        $this->setHeader( "Content-Type: multipart/form-data");
        $this->setOption(CURLOPT_URL, $this->getBaseUrl().$url);
        $this->setOption(CURLOPT_POSTFIELDS, $data);

        $this->exec();

        return $this->getResponse();
    }

    /**
     * init curl
     * construi array option avec header
     * enrefiste la reponse ou l'erreur
     * return un json + code HTTP
     *
     * @return array|mixed
     */
    public function exec()
    {
        $this->setCurl(curl_init());

        $this->setOption( CURLOPT_RETURNTRANSFER , true);
        $this->setOption(CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt_array($this->getCurl(), $this->getOptions());

        $this->setResult(curl_exec($this->getCurl()));
        $this->setErrors(curl_error($this->getCurl()));
        $this->setCodeHttp(curl_getinfo($this->getCurl()));
        $this->close($this->getCurl());


        if ($this->getErrors()) {
            $this->setResponse($this->getErrors(),$this->getCodeHttp());

        } else {
            $this->setResponse($this->getResult(),$this->getCodeHttp());
        }

    }


    /**
     * @return mixed
     */
    public function getCodeHttp()
    {
        return $this->codeHttp["http_code"];
    }

    /**
     * @param Integer $codeHttp
     */
    public function setCodeHttp($codeHttp)
    {
        $this->codeHttp = $codeHttp;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;

    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;

    }

    /**
     * @param mixed $response
     */
    public function setResponse($response, $status)
    {


        if ( $status == 200) {
            $this->response = [
                'reponse' => $response,
                'status' => $status
            ];



        } else {
            $this->response = [
                'erreur' => $response,
                'status' => $status
            ];
        }

    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @param $curl
     */
    public function close($curl)
    {
        curl_close($curl);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Ajouter une option curl a executer
     *
     * @param  $option
     * @param mixed $value
     *
     */
    public function setOption($option, $value)
    {
        $this->options  += [$option=> $value ];
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param string $header
     */
    public function setHeader(string $header)
    {
        array_push($this->headers,  $header);
    }

    /**
     * @return mixed
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @param mixed $curl
     */
    public function setCurl($curl)
    {
        dump($curl);
        $this->curl = $curl;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $url
     */
    public function setBaseUrl(string $url)
    {
        $this->baseUrl = $url;
    }


}